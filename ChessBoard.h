﻿#pragma once

class ChessBoard
{
public:
    
    unsigned long long GetKingMask(unsigned long long KingPos);
    unsigned long long GetKnightMask(unsigned long long KnightPos);
    
    void FindNextKingPositions(int CurrentPosition, unsigned long long& NextPositions);
    void FindNextKnightPositions(int CurrentPosition, unsigned long long& NextPositions);
    
    void FindCountOfBits(unsigned long long Position, int& CountOfBits);
    void FindCountOfBitsFirstMethod(unsigned long long Position, int& CountOfBits);

private:

    unsigned long long ConvertPositionToBits(int Position); 

    const unsigned long long LeftSideMask = 0xfefefefefefefefe;
    const unsigned long long RightSideMask = 0x7f7f7f7f7f7f7f7f;
    const unsigned long long DoubleLeftSideMask = 0xfcfcfcfcfcfcfcfc;
    const unsigned long long DoubleRightSideMask = 0x3f3f3f3f3f3f3f3f;
    
};
