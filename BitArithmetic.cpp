

#include <iostream>
#include "ChessBoard.h"

using namespace std;

int main(int argc, char* argv[])
{
    
    unsigned long long Positions;
    int CountOfBits = 0;
    int ChessBoardPositionIndex = 0;
    
    ChessBoard* ChessBoardRef = new ChessBoard;
    
    ChessBoardRef->FindNextKingPositions(ChessBoardPositionIndex, Positions);
    ChessBoardRef->FindCountOfBits(Positions, CountOfBits); 
    cout << Positions << endl << CountOfBits << endl;

    ChessBoardRef->FindNextKnightPositions(ChessBoardPositionIndex, Positions);
    ChessBoardRef->FindCountOfBits(Positions, CountOfBits); 
    cout << Positions << endl << CountOfBits << endl;
    
    return 0;
}
