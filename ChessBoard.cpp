﻿#include "ChessBoard.h"

unsigned long long ChessBoard::GetKingMask(unsigned long long KingPos)
{
    unsigned long long LeftSideKingMask = KingPos & LeftSideMask;
    unsigned long long RightSideKingMask = KingPos & RightSideMask;
    
    return  (LeftSideKingMask << 7) | (KingPos << 8) | (RightSideKingMask << 9) | (LeftSideKingMask >> 1)
             | (RightSideKingMask << 1) | (LeftSideKingMask >> 9) | (KingPos >> 8) | (RightSideKingMask >> 7);
}

void ChessBoard::FindNextKingPositions(int CurrentPosition, unsigned long long& NextPositions)
{
    unsigned long long KingPosition = ConvertPositionToBits(CurrentPosition);
    NextPositions = GetKingMask(KingPosition);
}

unsigned long long ChessBoard::GetKnightMask(unsigned long long KnightPos)
{
    unsigned long long LeftSideKnightMask = LeftSideMask & (KnightPos << 17 | KnightPos >> 15);
    unsigned long long RightSideKnightMask = RightSideMask &  (KnightPos << 15 | KnightPos >> 17);
    unsigned long long DoubleLeftKnightSideMask = DoubleLeftSideMask & (KnightPos << 10 | KnightPos >>  6);
    unsigned long long DoubleRightKnightSideMask = DoubleRightSideMask & (KnightPos <<  6 | KnightPos >> 10);
    
    return    LeftSideKnightMask | RightSideKnightMask | DoubleLeftKnightSideMask | DoubleRightKnightSideMask;
}

void ChessBoard::FindNextKnightPositions(int CurrentPosition, unsigned long long& NextPositions)
{
    unsigned long long KnightPosition = ConvertPositionToBits(CurrentPosition);
    NextPositions = GetKnightMask(KnightPosition);
}

void ChessBoard::FindCountOfBits(unsigned long long Position, int& CountOfBits)
{
    CountOfBits = 0;
    
    while (Position != 0)
    {
        Position &= Position - 1;
        CountOfBits++;
    }
}

void ChessBoard::FindCountOfBitsFirstMethod(unsigned long long Position, int& CountOfBits)
{
    CountOfBits = 0;
    
    while (Position != 0)
    {
        if ((Position & 1) == 1)
        {
            CountOfBits++;
        }

        Position >>= 1;
    }
}

unsigned long long ChessBoard::ConvertPositionToBits(int Position)
{
    return 1i64 << Position;
}
